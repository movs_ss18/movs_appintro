package com.example.steffen.movs_appintro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        Boolean firstStart = prefs.getBoolean("firstStart", true);

        if (firstStart) {
            prefs.edit().putBoolean("firstStart", false).commit();

            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
        }


        CheckBox cb = (CheckBox) findViewById(R.id.checkBox);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                prefs.edit().putBoolean("firstStart", isChecked).commit();
            }
        });

    }
}
