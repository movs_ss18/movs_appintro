package com.example.steffen.movs_appintro;

import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * Created by Steffen on 20.04.2017.
 */

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(SampleSlide.newInstance(R.layout.slide1));
        addSlide(SampleSlide.newInstance(R.layout.slide2));
        addSlide(AppIntroFragment.newInstance("We need Camera permission", "We want to take some pictures", R.drawable.ic_skip_white, getColor(R.color.colorAccent)));
        addSlide(SampleSlide.newInstance(R.layout.slide3));

        addSlide(AppIntroFragment.newInstance("Slide Title", "The looong description", R.drawable.ic_arrow_back_white, getColor(R.color.colorAccent)));


        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#51b53f"));
        setSeparatorColor(Color.parseColor("#a33fb5"));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);

        //Animation
        setFlowAnimation();

        //Permissions
        askForPermissions(new String[]{Manifest.permission.CAMERA}, 3);

    }


    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }



}
